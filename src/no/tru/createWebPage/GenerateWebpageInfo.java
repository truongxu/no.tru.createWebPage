package no.tru.createWebPage;

import no.tru.coursework.Course;
import no.tru.coursework.CourseWork;
import no.tru.coursework.CourseworkFactory;
import no.tru.coursework.CourseworkPackage;
import no.tru.coursework.Department;
import no.tru.coursework.EvaluationForm;
import no.tru.coursework.Person;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import javax.xml.parsers.ParserConfigurationException;


import org.eclipse.acceleo.query.delegates.AQLValidationDelegate;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EValidator.ValidationDelegate;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;


public class GenerateWebpageInfo {
	
	//-------------deciding variables--------------------------------
	private static String courseCode = "TDT4100"; //this is the course code to be displayed in the html file
	private static String xmiFile = "dep.xmi", html="webpage.html"; //these are the locations of .xmi and .html files
	//---------------------------------------------------------------
	
	private static File htmlFile= new File(html);
	private static CourseworkFactory factory = CourseworkFactory.eINSTANCE;
	static Document doc =null;
	static boolean foundCourse = false;
	private static Element eTemp;
	private static int lecturerCounter=1, evaformCount=0;
	
	private static void registerEMFStuff() {
		// registers the Transportation model's package object on its unique URI
		EPackage.Registry.INSTANCE.put(CourseworkPackage.eNS_PREFIX, CourseworkPackage.eINSTANCE);
		// register our Resource factory for the xmi extension, so the right Resource implementation is used
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		// register AQL (an OCL implementation) constraint support
		ValidationDelegate.Registry.INSTANCE.put("http://www.eclipse.org/acceleo/query/1.0", new AQLValidationDelegate());
		try {
			doc = (Document) Jsoup.parse(htmlFile, "UTF-8");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	private static void checkEClass( EObject eObject) throws IOException {
		Person person = factory.createPerson();
		EvaluationForm evaForm = factory.createEvaluationForm();
		CourseWork work = factory.createCourseWork();
		if(eObject.getClass()==person.getClass()){
			person = (Person) eObject;
			setPerson(person);
		}
		if(eObject.getClass()==evaForm.getClass()){
			evaForm = (EvaluationForm) eObject;
			setEvaForm(evaForm);
		}
		if(eObject.getClass()==work.getClass()){
			work = (CourseWork) eObject;
			setWork(work);
		}
	}
	
	private static void setWork( CourseWork work) {
		String tempString="";
		for (int i=0;i<4;i++) {
			eTemp=getElement(doc, "cw"+1);
			
			switch (i) {
			case 1: tempString="Teaching semeser: " + work.getCourseTerm();
				break;
			case 2: tempString="No.of lecture hours: " + work.getLectureHour();
				break;
			case 3: tempString="Lab hours: " + work.getLabhours();
				break;
			}
			
			eTemp.text(tempString);
		}
	}
	
	private static void setPerson(Person person) throws IOException {
		//start off with no lecturer(s)
		if(lecturerCounter<2) {
			for (int i =1; i<4;i++) {
				eTemp=getElement(doc, "lectures"+i);
				eTemp.empty();
			}
		}

		
		//check if person is coordinator or not
		if(person.eContainmentFeature().getName().equals("personCoordinator")) {
			eTemp=getElement(doc, "coodinator");
		}
		else {
			if(lecturerCounter<2) {
				eTemp = getElement(doc, "lectures"+lecturerCounter);
				eTemp.text("Lecturer(s)");
			}
			
			lecturerCounter +=1;
			eTemp=getElement(doc, "lectures"+lecturerCounter);
		}
		eTemp.text(person.getName());
	}
	
	private static void setEvaForm(EvaluationForm evaForm ) {
		if(evaformCount<1) {
			for (int i =1; i<11;i++) {
				eTemp=getElement(doc, "evaform"+i);
				eTemp.empty();
			}
		}
		
		for(int i = 0; i<2;i++) {
			if(evaformCount<3) {
				evaformCount += 1;
				eTemp=getElement(doc, "evaform"+evaformCount);
				if(evaformCount<2) {
					eTemp.text(evaForm.getType());
				}
				else {
					eTemp.text(evaForm.getWeight()*100+"/100");
					System.out.println(eTemp);
					evaformCount += 3;
				}
				
			}
			else {
				evaformCount += 1;
				eTemp=getElement(doc, "evaform"+evaformCount);
				if(evaformCount<7) {
					eTemp.text(evaForm.getType());
				}
				else {
					eTemp.text(evaForm.getWeight()*100+"/100");
				}
			}
		}
		
	}
	
	//finds right place to overwrite in HMTL file
	private static Element getElement(Document doc, String hmtlID) {
		return doc.getElementById(hmtlID);
	}
	
	//overwrite html file to update webpage
	private static void writeToFile(Document doc, File filename) throws IOException {
		Writer writer = new PrintWriter(filename);
		writer.write(doc.html());
		writer.close(); 
	}

	public static void main(String[] args) throws ParserConfigurationException, IOException {
		registerEMFStuff();
		
	
		// loading file
		ResourceSet rsSet = new ResourceSetImpl();
		URI uri1 = URI.createFileURI(xmiFile);
		Resource re = rsSet.getResource(uri1, true);
		re.load(null);
		EObject root = re.getContents().get(0);
		
		Course course = null;
		
		//set department 
		Department dep;
		dep = (Department) root;
		eTemp = getElement(doc, "dep");
		eTemp.text(dep.getName());
		
		
		//finds right course by courseCode
		for(int i =0;i<root.eContents().size();i++) {
			course = (Course) root.eContents().get(i);
			
			if(course.getCourseCode().equals(courseCode)) {
				foundCourse = true;
				break;
			}
		}
		
		
		if(foundCourse) {
			//sett courseName 
			eTemp = getElement(doc, "course");
			eTemp.text(course.getCourseCode()+"-"+course.getCourseName());
			eTemp = getElement(doc, "content");
			eTemp.text(course.getCourseDescription());
			//go through rest and add
			for (int i = 0;i<course.eContents().size();i++) {
				checkEClass(course.eContents().get(i));
			}
		}
		else {
			System.out.println(courseCode + " not found");
		}
		
		//update html file
		writeToFile(doc, htmlFile);
		
	}

}
