## Generate webpage from .xmi file
This project is using packages from no.tru.coursework. 
Other than org.eclipse.emf plug-ins I have used jsoup. A jar file is added to the project folder, add to libraries if needed. 

#Comments:
- dep.xmi -> this is the instance of the ecore model, takes the basis of one department with two courses (TDT4100 and TDT4250) and some information about those courses
- webpage.html -> this is the webpage made out of information from dep.xmi
- GenereateWebpageInfo.java -> this file opens up the dep.xmi and find information to pass to the hmtl file.
	- locations on both .xmi and .html files are necessary, this can be changed at the top of the file
	- course code decides on which to display, if the course code is not found, you will get to know it in the console window.
	- to switch out course, the variable "courseCode" needs to be changed and the java files needs to be ran as java application. 
